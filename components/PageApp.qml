import QtQuick 2.12
import QtQuick.Controls 2.12
import QtMultimedia 5.12

import "../js/config.js" as Cf

Rectangle {
    property bool isPortrait: this.width < this.height
    property bool toolButtonMenu: true
    property string title: qsTr("Sem Título")
    color: Cf.pageAppBackgroundColor

    anchors.left: parent ? parent.left : undefined
    anchors.leftMargin: drawer.width * drawer.position
}


