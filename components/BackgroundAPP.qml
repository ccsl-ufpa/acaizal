import QtQuick 2.12
import "../js/config.js" as Cf

Rectangle {
    color: Cf.backgroundColor

    Image {
        width: parent.width
        height: parent.height
        opacity: 0.7
        anchors.left: parent.left
        anchors.right: parent.right
        fillMode: Image.PreserveAspectCrop
        source: 'qrc:/static/images/image-background.png'
    }
}
