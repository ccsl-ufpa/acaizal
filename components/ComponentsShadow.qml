import QtGraphicalEffects 1.0

RectangularGlow {
    anchors.fill: parent
    glowRadius: 5
    spread: 0.2
    color: "gray"
    cornerRadius: parent.radius + glowRadius
    z: parent.z - 2
}
