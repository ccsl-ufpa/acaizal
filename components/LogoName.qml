import QtQuick 2.12
import QtQuick.Controls 2.12

Label {
    id: logoName
    color: 'white'
    text: qsTr(
        '<b>Açaizal</b>'
    )
}
