import QtQuick 2.12
import QtQuick.Controls 2.5
import QtQuick.Controls 2.12
import QtGraphicalEffects 1.3
import QtQuick.Layouts 1.3
import "js/config.js" as Cf
import "components"
import "Database/database.js" as Db

PageApp {
    id: blocoscopy

    title: qsTr("Área")

    StackView {
           anchors.fill: parent
    }
    TabBar {
        id: bar
        currentIndex: 0
        width: parent

        TabButton {
            id: palmeiras
            width: parent
            height: 40
            text: "Palmeiras"
            clip: true
        }
        TabButton {
            id: arvores
            width: palmeiras.width
            height: 40
            anchors.left: palmeiras.right
            text: "Árvores"
            clip: true
        }

    }

    StackLayout {
        anchors.fill: parent
        width: parent.width
        currentIndex: bar.currentIndex
    }
    Button {
        id: buttonAdd
        y: 432
        width: 48
        height: 40
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.horizontalCenter: parent.horizontalCenter3
        display: AbstractButton.IconOnly
        onClicked: popup.open()
        background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.width: 1
                radius: 2
                border.color: "#fbfbfb"
            }
        Image {
            id: iconAdd
            height: parent.height
            anchors.bottomMargin: -2
            anchors.horizontalCenterOffset: 0
            anchors.leftMargin: -40
            width: parent.width
            fillMode: Image.PreserveAspectFit
            anchors.left: parent.right
            anchors.topMargin: -4
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            source: "qrc:/Imagens/button-add.png"
        }
       }

    Popup {
        id: popup
        x: 100
        y: 100
        width: 300
        height: 450
        modal: true
        focus: true

        Column {
                id: coluna
                anchors.fill: parent
                anchors.margins: 10
                spacing: 10
                Label {
                    width: parent.width
                    elide: Label.ElideRight
                    text: "Selecione o tipo de de planta:*"
                }
                ComboBox {
                    id: comboBox
                    width: parent.width
                    model: ["Árvore", "Palmeira"]
                    onCurrentTextChanged: {
                        const sources = {
                            "Árvore": componentArvores,
                            "Palmeira": componentPalmeiras
                        }
                        loader.sourceComponent = sources[currentText]
                    }
                }
                Loader {
                    id: loader
                    width: parent.width
                }
                Component {
                    id: componentArvores
                    Column {
                        spacing: 10
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Selecione:*"
                        }
                        ComboBox {
                            id: comboArvore
                            width: parent.width
                            model: ["Andiroba", "Breu Branco", "Cacau", "Capoteiro", "Ceru", "Cinzento", "Goiaba Braba", "Guarajaí", "Inajarana", "Mututi", "PacaPeuá", "Papa-Terra", "Paranani", "Pracaxi", "Seringueira", "Taperebá", "Virola"]
                        }
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Quantidade de árvores médias:*"
                        }
                        TextField {
                            id: qtdArvMedia
                            width: parent.width
                        }
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Quantidade de árvores finas:*"
                        }
                        TextField {
                            id: qtdArvFina
                            width: parent.width
                        }
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Quantidade de árvores grossas:*"
                        }
                        TextField {
                            id: qtdArvGrossa
                            width: parent.width
                        }
                    }
                }
                Component {
                    id: componentPalmeiras
                    Column {
                        spacing: 10
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Selecione:*"
                        }
                        ComboBox {
                            id: comboPalmeira
                            width: parent.width
                            model: ["Buriti", "Murumuru", "Paxiúba"]
                        }
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Quantidade de palmeiras jovens:*"
                        }
                        TextField {
                            id: qtdPalmJovem
                            width: parent.width
                        }
                        Label {
                            width: parent.width
                            elide: Label.ElideRight
                            text: "Quantidade de palmeiras adultas:*"
                        }
                        TextField {
                            id: qtdPalmAdulta
                            width: parent.width
                        }
                    }
                }
            }
        ToolButton {
            id: cancelar
            x: 10
            y: 395
            text: qsTr("Cancelar")
            onClicked: popup.close()
        }

        ToolButton {
            id: salvar
            x: 196
            y: 395
            text: qsTr("Salvar")
            onClicked: {
                if(comboBox.currentText == "Árvore"){
                    Db.insertArvore(comboArvore, qtdArvFina, qtdArvMedia, qtdArvGrossa)
                } else {
                    Db.insertPalmeira(comboPalmeira, qtdPalmJovem, qtdPalmAdulta)
                }

            }
        }


        }

        //closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent

}

