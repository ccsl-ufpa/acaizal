import QtQuick 2.12

ListModel {
    id: modelModules
    ListElement {
        nameId: "home"
        active: true
        title: qsTr('Início')
        source: 'qrc:/0HomeForm.qml'
    }
    ListElement {
        nameId: "blocos"
        active: true
        title: qsTr('Blocos')
        source: 'qrc:/1Blocos.qml'
    }
    ListElement {
        nameId: "guia"
        active: true
        title: qsTr('Guia de Manejo')
        source: 'qrc:/2GuiaDeManejo.qml'
    }
    ListElement {
        nameId: "tutorial"
        active: true
        title: qsTr('Tutorial')
        source: 'qrc:/3Tutorial.qml'
    }
    ListElement {
        nameId: "perfil"
        active: true
        title: qsTr('Perfil')
        source: 'qrc:/4Perfil.qml'
    }
    ListElement {
        nameId: "contatos"
        active: true
        title: qsTr('Contatos')
        source: 'qrc:/5Contatos.qml'
    }
    ListElement {
        nameId: "sobre"
        active: true
        title: qsTr('Sobre')
        source: 'qrc:/6Sobre.qml'
    }
    ListElement {
        nameId: "areaBlocos"
        active: true
        title: qsTr('Área p/ teste')
        source: 'qrc:/1Blocos_copy.qml'
    }
}
