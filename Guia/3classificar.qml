import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../js/config.js" as Cf
import "../components"

PageApp {
    toolButtonMenu: false
    title: qsTr("Como Classificar")

    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 900
        antialiasing: false
        clip: true

        Text {
            id: classificando
            x: 0
            y: 13
            text: qsTr("Classificando as árvores e palmeiras")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Label {
            id: label1
            anchors.top: classificando.bottom
            text: qsTr("Deverão ser medidos os troncos das árvores com a fita métrica, para classificá-las da seguinte forma:
• Grossas: árvores maiores que 140 centímetros de roda.
• Médias: árvores entre 60 e 140 centímetros de roda.
• Finas: árvores entre 15 e 60 centímetros de roda.
As palmeiras de outras espécies (buriti, buçu, paxiúba, urucuri, etc.) deverão ser apenas contadas e classificadas da seguinte maneira:
• Jovens: plantas que ainda não produzem frutos.
• Adultas: plantas que estão produzindo frutos.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: classificando1
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label1.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/classificando.png"
        }
        Label {
            id: label2
            x: 0
            y: 266
            anchors.top: classificando1.bottom
            text: qsTr(" Os troncos das árvores e palmeiras devem ser medidos a 1,30 m de altura.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap

        }
    }
}
