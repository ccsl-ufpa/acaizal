import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../js/config.js" as Cf
import "../components"

PageApp {
    id: paginaApp
    toolButtonMenu: false

    title: qsTr("Como Limpar o Açaizal")
    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 1300
        antialiasing: false
        clip: true

        Text {
            id: primeiroPasso
            x: 0
            y: 13
            text: qsTr("Primeiro Passo")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Image {
            id: limpando1
            x: 174
            y: 8
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: primeiroPasso.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/primeiroPasso.png"
        }
        Label {
            id: label1
            anchors.top: limpando1.bottom
            text: qsTr(" Antes de fazer qualquer atividade no açaizal, o proprietário deve procurar a instituição estadual competente e solicitar as informações necessárias para obtenção da autorização de limpeza ou de manejo do açaizal nativo.
A autorização é fornecida pela instituição competente, após o proprietário apresentar os documentos e demais informações consideradas necessárias pela instituição.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }

        Text {
            id: limpando
            x: 0
            text: qsTr("Limpando o Açaizal")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
            anchors.topMargin: 18
            anchors.top: label1.bottom
        }
        Label {
            id: label2
            anchors.top: limpando.bottom
            text: qsTr("Caso necessário, o primeiro trabalho no açaizal deve ser a limpeza da área, por meio da roçagem e eliminação de cipós. ")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap

        }
        Image {
            id: limpando2
            x: 174
            y: 416
            width: 181
            height: 141
            anchors.horizontalCenterOffset: 3
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label2.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/limpando.png"
        }
        Text {
            id: naoEsquecer
            x: 12
            text: qsTr("Para não esquecer")
            anchors.topMargin: 16
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: limpando2.bottom
        }
        Label {
            id: label3
            x: 8
            color: "#26282b"
            text: qsTr("Um açaizal bem manejado deverá ter, em um hectare, mais ou menos:
• 400 touceiras (com 5 açaizeiros adultos em cada touceira).
• 50 palmeiras de outras espécies.
• 200 árvores.
Esta quantidade de plantas pode garantir alta produção de frutos e palmito, com uma alteração
mínima da biodiversidade.
A combinação adequada de árvores, açaizeiros e outras palmeiras bem distribuídos na área, além
de manter a diversidade florestal, é a chave para o sucesso do manejo do açaizal.")
            anchors.topMargin: 13
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
            anchors.top: naoEsquecer.bottom
    }


}
}





































/*##^## Designer {
    D{i:0;autoSize:true;height:480;width:640}
}
 ##^##*/
