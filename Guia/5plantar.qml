import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "../js/config.js" as Cf
import "../components"

PageApp {
    toolButtonMenu: false
    title: qsTr("Como Plantar")

    Flickable {
        id: flickable
        anchors.fill: parent
        anchors.rightMargin: anchors.leftMargin
        anchors.leftMargin: 10
        contentWidth: width
        height: 400
        anchors.bottomMargin: -259
        contentHeight: 900
        antialiasing: false
        clip: true

        Text {
            id: plantando
            x: 0
            y: 13
            text: qsTr("Plantando açaizeiros")
            anchors.horizontalCenter: parent.horizontalCenter
            horizontalAlignment: Text.AlignLeft
        }
        Label {
            id: label1
            anchors.top: plantando.bottom
            text: qsTr("Caso não tenham pés de açaí em número suficiente para a formação das 40 touceiras no bloco, deve ser feito o plantio de mudas ou sementes nos espaços abertos pelo corte das árvores.")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap
        }
        Image {
            id: plantando1
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label1.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/plantando.png"
        }
        Label {
            id: label2
            x: 0
            y: 266
            anchors.top: plantando1.bottom
            text: qsTr(" As sementes para o plantio deverão ser escolhidas de açaizeiros que apresentem alta produção e boa qualidade de frutos.
Continuando o trabalho de manejo do açaizal, novos blocos deverão ser demarcados na sequência do primeiro, seguindo as orientações das etapas relacionadas anteriormente")
            anchors.topMargin: 24
            font.family: "Times New Roman"
            fontSizeMode: Text.VerticalFit
            textFormat: Text.PlainText
            font.pixelSize: 14
            width: parent.width
            wrapMode: Label.Wrap

        }
        Image {
            id: plantando2
            width: 181
            height: 141
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.topMargin: 17
            anchors.top: label2.bottom
            fillMode: Image.PreserveAspectCrop
            source: "/Guia/GuiaImagens/plantando2.png"
        }
    }
}
