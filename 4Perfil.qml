import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import "js/config.js" as Cf
import "components"

PageApp {

    title: qsTr("Perfil")

    Label {
        text: qsTr("You are on Page 4.")
        anchors.centerIn: parent
    }
}
