﻿import QtQuick 2.12
import QtQuick.Controls 2.12
import QtQuick.LocalStorage 2.12
import QtGraphicalEffects 1.0
import "components"
import "models"
import "js/config.js" as Cf
import "qrc:/Database/database.js" as Db

ApplicationWindow {
    property variant currentItem

    id: root
    visible: true

    height: Cf.heigth
    width: Cf.width

    minimumHeight: Cf.heigth / 2
    minimumWidth: Cf.width / 2

    title: qsTr(Cf.title)

    header: ToolBarAPP {
        id: toolBar
        title: currentItem ? currentItem.title : ""
        toolButtonMenu: currentItem ? currentItem.toolButtonMenu : true
    }
    StackView {
        id: stackView
        anchors.fill: parent
        initialItem: "qrc:/0HomeForm.qml"
        onCurrentItemChanged:{
            root.currentItem = currentItem
        }
    }
    Shortcut {
        id: shortcutGlobal
        sequences: ["Esc", "Back"]
        onActivated: {
            if (stackView.depth === 1) {
                Qt.quit()
            } else {
                stackView.pop()
            }
        }
    }

    Drawer {
        id: drawer
        width: (Math.min(parent.width, parent.height) / 4) * 3
        height: parent.height
        dragMargin: Qt.styleHints.startDragDistance * 1.8
        interactive: root.currentItem.toolButtonMenu
        Pane {
            id: paneDrawer
            height: (parent.width * 0.5 < 100) ? parent.width * 0.5 : 100
            width: parent.width
            clip: true
            background: BackgroundAPP {}
            Image {
                id: logoImageDrawer
                anchors.verticalCenter: parent.verticalCenter
                anchors.left: parent.left
                anchors.margins: 5
                height: parent.height
                fillMode: Image.PreserveAspectFit
            }
            LogoName {
                font.pixelSize: parent.height * 0.3
                anchors.left: logoImageDrawer.right
                anchors.bottom: parent.bottom
                anchors.margins: 10
            }
        }
        ListView {
            property int initialIndex: 0
            id: listView
            anchors.top: paneDrawer.bottom
            boundsBehavior: Flickable.OvershootBounds
            currentIndex: initialIndex
            width: parent.width
            height: parent.height - paneDrawer.height
            z: paneDrawer.z - 1

            delegate: ItemDelegate {
                width: parent.width
                text: model.title
                icon.source: model.sourceIcon
                icon.color: Cf.backgroundColor
                opacity: model.active ? 1 : 0.4
                enabled: model.active

                onClicked: {
                    if (model.title !== currentItem.title) {
                        stackView.pop(null, StackView.PushTransition)
                        if (model.index !== listView.initialIndex)
                            stackView.push(model.source, {
                                               "title": title
                                           })
                    }

                    drawer.close()
                }
            }
            model: ModulesListModel {
                id: listModel
            }
        }
    }
    ToolTip {
        id: toolTip
        x: parent.width / 2 - width / 2
        y: parent.height - height * 2
        padding: 5
        timeout: 3000
        opacity: 0.8
        font.pixelSize: 12
        contentItem: Text {
            text: toolTip.text
            font: toolTip.font
            color: "white"
        }
        background: Rectangle {
            color: "black"
            radius: 10
        }
    }
    Component.onCompleted: {
           Db.createDatabase()
       }

}
