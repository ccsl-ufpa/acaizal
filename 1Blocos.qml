import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3
import QtQuick.LocalStorage 2.12
import QtQuick.Window 2.12
import "js/config.js" as Cf
import "components"
import "./Database/database.js" as Db

PageApp {
    id: paginaApp

    title: qsTr("Blocos")

    ListView {
        property int initialIndex: 0
        id: listArea
        layoutDirection: Qt.AlignCenter
        anchors.fill: parent
        anchors.centerIn: parent
        width: parent.width; height: parent.height

        model: ListModel {
            id: idListArea
            Component.onCompleted: Db.selecArea()

        }

        delegate: Component {
            id:  areaDelegate
            Item {
                id: itemDelegate
                width:  493;  height:  126
                Rectangle {
                    id: box
                    height:100
                    width: parent.width
                    color: "#F0FFFF"
                    border.width: 2
                    border.color: "black"
                    radius: 5
                Column {
                    id: column
                    x: 0
                    y: 0
                    width: 493
                    height: 100
                    anchors.right: parent
                    anchors.rightMargin: 53
                    rightPadding: 3
                    padding: 3
                    topPadding: 4
                    leftPadding: 5
                    spacing: 7

                    MouseArea {
                        width: 492
                        height: 123
                        anchors.bottomMargin: 25
                        hoverEnabled: false
                        anchors.fill: parent
                        onClicked: {
                            pageLoader.source = "qrc:/1Blocos_copy.qml"
                        }
                    }

                    Button {
                        id: buttonExcluir
                        x: 457
                        y: 35
                        width: 21
                        height: 32
                        anchors.right: nomeComprimento
                        anchors.bottom: nomeComprimento
                        display: AbstractButton.IconOnly
                        onClicked: popExcluir.open()
                        background: Rectangle {
                            id: ex
                                implicitWidth: 100
                                implicitHeight: 40
                                opacity: enabled ? 1 : 0.3
                                border.width: 1
                                radius: 2
                                border.color: "#fbfbfb"
                            }
                        Image {
                            id: iconExcluir
                            x: -11
                            width: 32
                            height: 32
                            anchors.verticalCenterOffset: 0
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenterOffset: 1
                            fillMode: Image.PreserveAspectFit
                            anchors.top: parent.right
                            anchors.horizontalCenter: parent.horizontalCenter
                            anchors.bottom: parent.right
                            source: "qrc:/Imagens/lixeira.svg"
                        }
                    }
                    Button {
                        id: editarArea
                        x: -14
                        y: 0
                        width: 32
                        height: 32
                        topPadding: 4
                        leftPadding: 7
                        anchors.verticalCenterOffset: -12
                        anchors.horizontalCenterOffset: 192
                        anchors.horizontalCenter: parent.horizontalCenter
                        anchors.verticalCenter: parent.verticalCenter
                        display: AbstractButton.IconOnly
                        onClicked: popExcluir.open()
                        background: Rectangle {
                            id: editRect
                                implicitWidth: 100
                                implicitHeight: 40
                                opacity: enabled ? 1 : 0.3
                                border.width: 1
                                radius: 2
                                border.color: "#fbfbfb"
                            }
                        Image {
                            id: iconEditar
                            x: -11
                            y: 0
                            width: 32
                            height: 32
                            anchors.verticalCenterOffset: 14
                            anchors.horizontalCenterOffset: 4
                            anchors.verticalCenter: parent.verticalCenter
                            anchors.horizontalCenter: parent.horizontalCenter
                            fillMode: Image.PreserveAspectFit
                            anchors.top: parent.right
                            anchors.bottom: parent.right
                            source: "qrc:/Imagens/editarArea.svg"
                        }
                    }

                    Text {
                        id: nomeComprimento
                        x: 4
                        y: 12
                        width: 426
                        height: 14
                        text: '<b>Nome:</b>' + nome_area + ' ' + '<b>Comprimento:</b>' + String(comprimento)

                    }
                    Text {
                        x: 4
                        y: 34
                        height: 13
                        text: '<b>Largura:</b>' + String(largura)
                        horizontalAlignment: Text.AlignLeft
                }
                    Text {
                        x: 4
                        y: 76
                        height: 14
                      text: '<b>Área Total:</b>' + comprimento * largura
                    }
                    Text {
                        x: 4
                        y: 56
                        height: 13
                      text:  (descricao !=null) ? '<b>Descrição:</b>' + descricao : "</br>"
                    }
            }}}
        }
    }

    Popup {
        id: popExcluir
        width: 160
        height: 160
        modal: true
        focus: true
        contentItem: Rectangle {
            width: 160
            height: 160
               Text {
                   id: textConfirme
                   y: 8
                   text: qsTr("Confirmação")
                   anchors.horizontalCenterOffset: 1
                   font.pixelSize: 17
                   font.family: "Times"
                   horizontalAlignment: Text.AlignHCenter
                   anchors.horizontalCenter: parent.horizontalCenter
               }
               Label {
                  id: labelPergunta
                  x: 0
                  anchors.top: textConfirme.bottom
                  width: parent.width
                  wrapMode: Label.Wrap
                  text: qsTr("Você deseja realmente excluir?")
                  horizontalAlignment: Text.AlignHCenter
                  anchors.topMargin: 24
               }
               Button {
                   id: confirma
                   x: 99
                   width: 61
                   height: 24
                   anchors.top: labelPergunta.bottom
                   anchors.right: parent.right
                   text: "Sim"
                   font.preferShaping: false
                   hoverEnabled: false
                   anchors.rightMargin: 0
                   anchors.topMargin: 25
                   display: AbstractButton.TextOnly
                   enabled: !creatingNewEntry && listArea.currentIndex //!== -1
                   onClicked: {
                       Db.removeArea(listArea.model.get(listArea.currentIndex).id)
                       listArea.model.remove(listArea.currentIndex)
                       popExcluir.close()
                       /*if (listArea.count == 0) {
                            listArea.currentIndex = -1
                       }*/
                   }

               }
               ToolButton {
                   id: cancel
                   x: -7
                   y: 105
                   width: 70
                   height: 25
                   text: qsTr("Cancelar")
                   onClicked: popExcluir.close()
               }
        }
        closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
    }

    Button {
        id: buttonAdd
        y: 432
        width: 48
        height: 40
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 8
        anchors.horizontalCenter: parent.horizontalCenter3
        display: AbstractButton.IconOnly
        onClicked: popup.open()
        background: Rectangle {
                implicitWidth: 100
                implicitHeight: 40
                opacity: enabled ? 1 : 0.3
                border.width: 1
                radius: 2
                border.color: "#fbfbfb"
            }
        Image {
            id: iconAdd
            height: parent.height
            anchors.bottomMargin: -2
            anchors.horizontalCenterOffset: 0
            anchors.leftMargin: -40
            width: parent.width
            fillMode: Image.PreserveAspectFit
            anchors.left: parent.right
            anchors.topMargin: -4
            anchors.top: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            source: "qrc:/Imagens/button-add.png"
        }
       }

       Popup {
           id: popup
           x: 100
           y: 100
           width: 300
           height: 400
           modal: true
           focus: true
           contentItem: Rectangle {
                  Text {
                      id: textAviso
                      text: qsTr("Informação")
                      font.pixelSize: 17
                      font.family: "Times"
                      horizontalAlignment: Text.AlignHCenter
                      anchors.horizontalCenter: parent.horizontalCenter
                  }
                  Label {
                     id: labelAviso
                     x: 0
                     anchors.top: textAviso.bottom
                     width: parent.width
                     wrapMode: Label.Wrap
                     text: qsTr(" Antes de fazer qualquer atividade no açaizal, o proprietário deve procurar a instituição estadual competente e solicitar as informações necessárias para obtenção da autorização de limpeza ou de manejo do açaizal nativo.
A autorização é fornecida pela instituição competente, após o proprietário apresentar os documentos e demais informações consideradas necessárias pela instituição.")
                     anchors.topMargin: 15
                  }
                  Button {
                      x: 167
                      anchors.top: labelAviso.bottom
                      anchors.right: parent.right
                      text: "Ok, entendi."
                      font.preferShaping: false
                      hoverEnabled: false
                      anchors.rightMargin: 8
                      anchors.topMargin: 135
                      display: AbstractButton.TextOnly
                      onClicked: popup2.open()
                  }
                  ToolButton {
                      id: sair
                      x: 8
                      y: 327
                      text: qsTr("Sair")
                      anchors.top: control3
                      onClicked: popup.close()
                  }
           }

           closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
       }
       Popup {
           id: popup2
           x: 100
           y: 100
           width: 300
           height: 400
           modal: true
           focus: true
           contentItem: Rectangle {
               Text {
                   id: element
                   x: 8
                   y: 5
                   text: qsTr("Digite um nome para a sua área: *")
                   font.pixelSize: 12
               }

               TextArea {
                   id: control1
                   x: 8
                   y: 26
                   width: 261
                   height: 27
                   placeholderText: qsTr("Insira o nome")
                   anchors.top: element
                   background: Rectangle {
                       implicitWidth: 200
                       implicitHeight: 40
                       border.color: control1.enabled ? "#9932CC" : "transparent"
                   }
               }

               Text {
                   id: element1
                   x: 8
                   y: 59
                   text: qsTr("Comprimento: *")
                   font.pixelSize: 12
               }

                TextField {
                    id: control2
                    x: 8
                    y: 80
                    width: 261
                    height: 27
                    anchors.topMargin: 60
                    renderType: Text.QtRendering
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control2.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    placeholderText: qsTr("Insira o comprimento")
                    inputMethodHints: Qt.ImhDigitsOnly
                                validator: DoubleValidator {
                                    bottom: 0
                                }
                }

                Text {
                    id: element2
                    x: 8
                    y: 113
                    text: qsTr("Largura: *")
                    font.pixelSize: 12
                }

                TextField {
                    id: control3
                    x: 8
                    y: 134
                    width: 261
                    height: 27
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control3.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    placeholderText: qsTr("Insira a largura")
                    inputMethodHints: Qt.ImhDigitsOnly
                                validator: DoubleValidator {
                                    bottom: 0
                                }
                    renderType: Text.QtRendering
                    anchors.top: element
                }
                Text {
                    id: element3
                    x: 8
                    y: 167
                    text: qsTr("Tamanho da área em ha:")
                    font.pixelSize: 12
                }
                TextField {
                    x: 8
                    y: 188
                    width: 261
                    height: 29
                    background: Rectangle {
                        color: "transparent"
                        border.color: "#9932CC"
                        radius: 2
                        }
                    text: control2.text * control3.text
                    readOnly: true
                    inputMethodHints: Qt.ImhDigitsOnly
                    }

                Text {
                    id: element4
                    x: 8
                    y: 223
                    text: qsTr("Informação adicional (Opcional): ")
                    font.pixelSize: 12
                }
                TextArea {
                    id: control4
                    x: 8
                    y: 244
                    width: 261
                    height: 27
                    background: Rectangle {
                        implicitHeight: 40
                        border.color: control3.enabled ? "#9932CC" : "transparent"
                        implicitWidth: 200
                    }
                    renderType: Text.QtRendering
                    anchors.top: element
                }
                ToolButton {
                    id: cancelar
                    x: 8
                    y: 327
                    text: qsTr("Cancelar")
                    anchors.top: control3
                    onClicked: popup2.close()
                }

                ToolButton {
                    id: salvar
                    x: 196
                    y: 327
                    width: 71
                    height: 40
                    text: qsTr("Salvar")
                    anchors.top: control3
                    onClicked: {
                        Db.insertArea(control1.text, control2.text, control3.text, control4.text)
                        Db.selecArea()
                        control1.clear()
                        control2.clear()
                        control3.clear()
                        control4.clear()

                    }
                }



           }

           closePolicy: Popup.CloseOnEscape | Popup.CloseOnPressOutsideParent
       }
       Loader {
           id: pageLoader
           anchors.fill: parent
       }
}

