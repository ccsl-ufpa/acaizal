﻿function createDatabase()
{
    try {
        var db = LocalStorage.openDatabaseSync("acaizal", "1.0",
                                               "banco de dados do app Açaizal", 1000000)

        db.transaction(function (tx) {
            var sql = ('CREATE TABLE IF NOT EXISTS Acaizal_Area (id INTEGER PRIMARY KEY AUTOINCREMENT, nome_area TEXT NOT NULL, comprimento REAL NOT NULL, largura REAL NOT NULL, descricao TEXT)');
            tx.executeSql(sql);
        })
    } catch (err) {
        console.log("Error creating table in database: " + err)
    }
    return db
}

function getDatabase()
{
    try {
        var db = LocalStorage.openDatabaseSync("acaizal", "1.0",
                                               "banco de dados de pessoas", 1000000)
    } catch (err) {
        console.log("Error opening database: " + err)
    }
    return db
}
        //Tabela Area
function getArea(tx){
                var db = getDatabase();
                db.transaction(function (tx) {
                    var sql = ('SELECT id, nome_area, comprimento, largura, descricao FROM Acaizal_Area');
                    var rs = tx.executeSql(sql);
                    var id, nome_area, comprimento, largura, descricao, ix;
                    for (ix = 0; ix < rs.rows.length; ix++){
                        id = rs.rows.item(ix).id;
                        nome_area = rs.rows.item(ix).nome_area;
                        comprimento = rs.rows.item(ix).comprimento;
                        largura = rs.rows.item(ix).largura;
                        descricao = rs.rows.item(ix).descricao;
                    }
                    //return rs
                })
                    return tx
            }

        //Tabela Plantação Árvore
function createPArvore(){
        db.transaction(function (tx){
                        var sql = ('CREATE TABLE IF NOT EXISTS Acaizal_PlantacaoArvore (id INTEGER PRIMARY KEY AUTOINCREMENT, nome_arvore NOT NULL, qtd_arvore_fina INTEGER NOT NULL, qtd_arvore_media INTEGER NOT NULL, qtd_arvore_grossa INTEGER NOT NULL)');
                        tx.executeSql(sql);
        })
}
function getPArvore(tx){
        db.transaction(
                    function(tx){
                        var sql = 'SELECT id, nome_arvore, qtd_arvore_fina, qtd_arvore_media, qtd_arvore_grossa FROM Acaizal_PlantacaoArvore';
                        var rs = tx.executeSql(sql);
                        var id, nome_arvore, qtd_arvore_fina, qtd_arvore_media, qtd_arvore_grossa, ix;
                        for (ix = 0; ix < rs.rows.length; ++ix){
                            id = rs.rows.item(ix).id;
                            nome_arvore = rs.rows.item(ix).nome_area;
                            qtd_arvore_fina = rs.rows.item(ix).qtd_arvore_fina;
                            qtd_arvore_media = rs.rows.item(ix).cqtd_arvore_media;
                            qtd_arvore_grossa = rs.rows.item(ix).qtd_arvore_grossa;
                        }
                    })
}

        //Tabela Plantação Palmeira
function createPPalmeira(){
        db.transaction(function (tx){
                        var sql = ('CREATE TABLE IF NOT EXISTS Acaizal_PlantacaoPalmeira (id INTEGER PRIMARY KEY AUTOINCREMENT, nome_palmeira NOT NULL, qtd_palmeira_jovem INTEGER NOT NULL, qtd_palmeira_adulta INTEGER NOT NULL)');
                        tx.executeSql(sql);
                    })
}
function getPPalmeira(tx){
        db.transaction(function(tx){
                        var sql = 'SELECT id, nome_palmeira, qtd_palmeira_jovem, qtd_palmeira_adulta FROM Acaizal_PlantacaoPalmeira';
                        var rs = tx.executeSql(sql);
                        var id, nome_palmeira, qtd_palmeira_jovem, qtd_palmeira_adulta, ix;
                        for (ix = 0; ix < rs.rows.length; ++ix){
                            id = rs.rows.item(ix).id;
                            nome_palmeira = rs.rows.item(ix).nome_palmeira;
                            qtd_palmeira_jovem = rs.rows.item(ix).qtd_palmeira_jovem;
                            qtd_palmeira_adulta = rs.rows.item(ix).qtd_palmeira_adulta;
                        }
                    })
}

        //Tabela Plantação Touceira
function createPTouceira(){
        db.transaction(function (tx){
                        var sql = ('CREATE TABLE IF NOT EXISTS Acaizal_PlantacaoTouceira (id INTEGER PRIMARY KEY AUTOINCREMENT, qtd_palmeira_jovem INTEGER NOT NULL, qtd_palmeira_adulta INTEGER NOT NULL)');
                        tx.executeSql(sql);
                    })
}
function getPTouceira(tx){
        db.transaction(function(tx){
                        var sql = 'SELECT id, qtd_touceira_jovem, qtd_touceira_adulta FROM Acaizal_PlantacaoTouceira';
                        var rs = tx.executeSql(sql);
                        var id, qtd_touceira_jovem, qtd_touceira_adulta, ix;
                        for (ix = 0; ix < rs.rows.length; ++ix){
                            id = rs.rows.item(ix).id;
                            qtd_touceira_jovem = rs.rows.item(ix).qtd_touceira_jovem;
                            qtd_touceira_adulta = rs.rows.item(ix).qtd_touceira_adulta;
                        }
                    })
}

        //Tabela Tipo de Planta
function createTipo(){
        db.transaction(function (tx){
                        var sql = ('CREATE TABLE IF NOT EXISTS Acaizal_Tipo (id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, tipo_planta INTEGER NOT NULL, planta INTEGER NOT NULL, area_id INTEGER NOT NULL, arvore_id INTEGER NOT NULL, palmeira_id INTEGER NOT NULL, touceira_id INTEGER NOT NULL, constraint "FK_PlantacaoArvore" foreign key (arvore_id) references acaizal_plantacaoarvore (id), constraint "FK_PlantacaoPalmeira" foreign key (palmeira_id) references acaizal_plantacaopalmeira (id), constraint "FK_PlantacaoTouceira" foreign key (touceira_id) references acaizal_plantacaotouceira (id), constraint "FK_PlantacaoArea" foreign key (area_id) references acaizal_area(id))');
                      tx.executeSql(sql);
                    })
}

function getTipo(tx){
        db.transaction(function(tx){
                        var sql = 'SELECT id, tipo_planta, planta, area_id, arvore_id, palmeira_id, touceira_id FROM Acaizal_Tipo';
                        var rs = tx.executeSql(sql);
                        var id, tipo_planta, planta, area_id, arvore_id, palmeira_id, touceira_id, ix;
                        for (ix = 0; ix < rs.rows.length; ++ix){
                            id = rs.rows.item(ix).id;
                            tipo_planta = rs.rows.item(ix).tipo_planta;
                            planta = rs.rows.item(ix).planta;
                            area_id = rs.rows.item(ix).area_id;
                            arvore_id = rs.rows.item(ix).arvore_id;
                            palmeira_id = rs.rows.item(ix).palmeira_id;
                            touceira_id = rs.rows.item(ix).touceira_id;
                        }
                    })
}
function insertArea(control1, control2, control3, control4){
    var db = getDatabase()
    var id
    console.log(control1, control2, control3, control4)
    db.transaction(function(tx){
        tx.executeSql('INSERT INTO Acaizal_Area (nome_area, comprimento, largura, descricao) VALUES (?, ?, ?, ?)', [control1, control2, control3, control4])
        console.log("Área " + control1 + " inserida!")
        var result = tx.executeSql('SELECT * FROM Acaizal_Area WHERE id = ?', [id])
    })
    return id
}
//Pra quando for feita a modificação dos dados da área
function AreaUpdate(control1, control2, control3, control4)
{
    var db = getArea()
    db.transaction(function (tx) {
        tx.executeSql('update Acaizal_Area (nome_area, comprimento, largura, descricao) VALUES (?, ?, ?, ?)', [control1, control2, control3, control4])
        console.log("Update da área " + control1 + " feito!")
    })
}

function insertArvore(comboArvore, qtdArvFina, qtdArvMedia, qtdArvGrossa){
    var db = getPArvore()
    db.transaction(function(tx){
        tx.executeSql('INSERT INTO Acaizal_PlantacaoArvore (nome_arvore, qtd_arvore_fina, qtd_arvore_media, qtd_arvore_grossa) VALUES (?,?,?,?)', [comboArvore, qtdArvFina, qtdArvMedia, qtdArvGrossa])
        console.log("Árvore " + comboArvore + " inserida!")
    })
}
function selectArvore(){
                var db = getPArvore();
                idListArea.clear()
                db.transaction(function (tx) {
                    var sql = ('SELECT id, nome_arvore, qtd_arvore_fina, qtd_arvore_media, qtd_arvore_grossa FROM Acaizal_PlantacaoArvore');
                    var rs = tx.executeSql(sql);
                    var ix;
                    for (ix = 0; ix < rs.rows.length; ix++){
                        console.log(rs.rows.item(ix).nome_area, rs.rows.item(ix).qtd_arvore_fina, rs.rows.item(ix).qtd_arvore_media, rs.rows.item(ix).qtd_arvore_grossa);
                        idListArea.append({
                             id: rs.rows.item(ix).id,
                             nome_area: rs.rows.item(ix).nome_area,
                             qtd_arvore_fina: rs.rows.item(ix).qtd_arvore_fina,
                             qtd_arvore_media: rs.rows.item(ix).qtd_arvore_media,
                             qtd_arvore_grossa: rs.rows.item(ix).qtd_arvore_grossa
                        })
                    }
                })

            }
function insertPalmeira(comboPalmeira, qtdPalmJovem, qtdPalmAdulta){
    var db = getPPalmeira()
    db.transaction(function(tx){
        tx.executeSql('INSERT INTO Acaizal_PlantacaoPalmeira (nome_palmeira, qtd_palmeira_jovem, qtd_palmeira_adulta) VALUES (?,?,?)', [comboPalmeira, qtdPalmJovem, qtdPalmAdulta])
        console.log("Palmeira " + comboPalmeira + " inserida!")
    })
}
function selectPalmeira(){
                var db = getPPalmeira();
                idListArea.clear()
                db.transaction(function (tx) {
                    var sql = ('SELECT id, nome_palmeira, qtd_palmeira_jovem, qtd_palmeira_adulta FROM Acaizal_PlantacaoPalmeira');
                    var rs = tx.executeSql(sql);
                    var ix;
                    for (ix = 0; ix < rs.rows.length; ix++){
                        console.log(rs.rows.item(ix).nome_area, rs.rows.item(ix).qtd_palmeira_jovem, rs.rows.item(ix).qtd_palmeira_adulta);
                        idListArea.append({
                             id: rs.rows.item(ix).id,
                             nome_area: rs.rows.item(ix).nome_area,
                             qtd_palmeira_jovem: rs.rows.item(ix).qtd_palmeira_jovem,
                             qtd_palmeira_adulta: rs.rows.item(ix).qtd_palmeira_adulta
                        })
                    }
                })

            }
function insertTouceira(){
    var db = getPPalmeira()
    db.transaction(function(tx){
        tx.executeSql('INSERT INTO Acaizal_PlantacaoTouceira (qtd_palmeira_jovem, qtd_palmeira_adulta) VALUES (?,?)', [])
    })
}

function selecArea(){
                var db = getDatabase();
                idListArea.clear()
                db.transaction(function (tx) {
                    var sql = ('SELECT id, nome_area, comprimento, largura, descricao FROM Acaizal_Area');
                    var rs = tx.executeSql(sql);
                    var ix;
                    for (ix = 0; ix < rs.rows.length; ix++){
                        console.log(rs.rows.item(ix).nome_area, rs.rows.item(ix).comprimento, rs.rows.item(ix).largura, rs.rows.item(ix).descricao);
                        idListArea.append({
                             id: rs.rows.item(ix).id,
                             nome_area: rs.rows.item(ix).nome_area,
                             comprimento: rs.rows.item(ix).comprimento,
                             largura: rs.rows.item(ix).largura,
                             descricao: rs.rows.item(ix).descricao
                        })
                    }
                })

            }
function removeArea(numid){
                var db = getDatabase();
                db.transaction(function (tx) {
                    var rs = tx.executeSql('delete FROM Acaizal_Area WHERE id = ?', [numid])
                    console.log("Área " + numid)
                    for (var ix = 0; ix < rs.rows.length; ix++){
                        console.log(rs.rows.item(ix).nome_area, rs.rows.item(ix).comprimento, rs.rows.item(ix).largura, rs.rows.item(ix).descricao);
                        idListArea.remove({
                             id: rs.rows.item(ix).id,
                             nome_area: rs.rows.item(ix).nome_area,
                             comprimento: rs.rows.item(ix).comprimento,
                             largura: rs.rows.item(ix).largura,
                             descricao: rs.rows.item(ix).descricao
                        })
                    }
            })
}
