import QtQuick 2.12
import QtQuick.Controls 2.5
import QtGraphicalEffects 1.3

import "components"
import "js/config.js" as Cf

PageApp {
    property int heightTitles: 30
    property string colorsTitles: Cf.backgroundColor.replace('#', '#e6')

    id: page
    title: qsTr("Sobre")

    Text {
        id: text0
        x: 0
        y: 9
        text: "Este aplicativo teve como base as seguintes referências:"
        horizontalAlignment: Text.AlignHCenter
        font.bold: true
        anchors.topMargin: 21
        font.family: "Times New Roman"
        fontSizeMode: Text.VerticalFit
        textFormat: Text.PlainText
        font.pixelSize: 16
        width: parent.width
        color: "#820cb3"
        wrapMode: Label.Wrap
    }

    Label {
        id: label
        text: "Guia prático de manejo de açaizais para produção de frutos / José Antonio Leite de Queiroz, Silas Mochiutti; ilustração de Marcos Antonio da Silva. – 2. ed. rev. amp. - Macapá: Embrapa Amapá, 2012."
        anchors.left: parent.left
        anchors.leftMargin: 9
        anchors.right: parent.right
        anchors.rightMargin: 8
        visible: true
        anchors.topMargin: 14
        font.family: "Times New Roman"
        fontSizeMode: Text.VerticalFit
        textFormat: Text.PlainText
        font.pixelSize: 16
        height: 35
        wrapMode: Label.Wrap
        anchors.top: text0.bottom
    }
}









/*##^##
Designer {
    D{i:0;autoSize:true;height:480;width:640}D{i:2;anchors_width:640;anchors_x:-8}
}
##^##*/
