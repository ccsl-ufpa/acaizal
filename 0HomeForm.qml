import QtQuick 2.12
import QtQuick.Controls 2.5

import "components"

PageApp {
    title: qsTr("Início")

    id: page

    Image {
        id: logo
        width: 181
        height: 141
        z: 1
        fillMode: Image.PreserveAspectCrop
        source: "qrc:/Imagens/images.jpg"
        anchors.centerIn: parent
    }

    Text {
        id: nomeLogo
        width: 172
        height: 68
        color: "#712ddb"
        font.pointSize: 21
        text: qsTr("Meu Açaizal")
        anchors.verticalCenterOffset: 115
        anchors.horizontalCenterOffset: 0
        anchors.verticalCenter: parent.verticalCenter
        anchors.horizontalCenter: parent.horizontalCenter
        font.underline: false
        font.italic: true
        fontSizeMode: Text.Fit
        font.wordSpacing: -0.1
        font.bold: true
        font.family: "Times New Roman"
        elide: Text.ElideRight
        renderType: Text.NativeRendering
        textFormat: Text.AutoText
        verticalAlignment: Text.AlignVCenter
        horizontalAlignment: Text.AlignHCenter
    }
}




